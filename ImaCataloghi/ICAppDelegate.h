//
//  ICAppDelegate.h
//  ImaCataloghi
//
//  Created by Francesco Frascà on 12/03/14.
//  Copyright (c) 2014 Francesco Frascà e Alberto Misuraca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ASIHTTPRequest/ASIHTTPRequest.h>

@interface ICAppDelegate : UIResponder <UIApplicationDelegate, ASIHTTPRequestDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSString *userId;
@property (strong, nonatomic) NSString *deviceToken;
@property (assign, nonatomic) BOOL registered;
@end
