//
//  ICCatalogoCell.h
//  ImaCataloghi
//
//  Created by Francesco Frascà on 12/03/14.
//  Copyright (c) 2014 Francesco Frascà e Alberto Misuraca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICConstants.h"
@class ICCatalogo;

@interface ICCatalogoCell : UITableViewCell
{
    ICCatalogo* _catalogo;
    Status _status;
}

@property (nonatomic, strong) ICCatalogo* catalogo;
@property (nonatomic, assign) Status status;

#pragma mark IB
@property (weak, nonatomic) IBOutlet UILabel *labelTitolo;
@property (weak, nonatomic) IBOutlet UILabel *labelData;
@property (weak, nonatomic) IBOutlet UILabel *labelAttesa;
@property (weak, nonatomic) IBOutlet UILabel *labelProgress;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewStatus;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *progressIndicator;

@end
