//
//  ICConstants.h
//  ImaCataloghi
//
//  Created by Francesco Frascà on 12/03/14.
//  Copyright (c) 2014 Francesco Frascà e Alberto Misuraca. All rights reserved.
//

#import <Foundation/Foundation.h>

#define MAIN_URL @"http://192.168.178.33:8888/ImaCataloghi/"

// Status enum
typedef enum {
    ICStatusToDownload,
    ICStatusDownloaded,
    ICStatusInAttesa,
    ICStatusDownloading
} Status;

@interface ICConstants : NSObject

// Dictionary Key
extern NSString * const ICKeyTitolo;
extern NSString * const ICKeyData;
extern NSString * const ICKeyURL;
extern NSString * const ICKeyDownloaded;

extern NSString * const ICKeyUserId;
extern NSString * const ICKeyDeviceToken;
extern NSString * const ICKeyRegistered;

// File Names
extern NSString * const ICFileNamePlist;
extern NSString * const ICFileNamePlistWithExtension;
extern NSString * const ICFileNameDownloadImage;
extern NSString * const ICFileNameCheckImage;

// URL
extern NSString * const ICURLApi;


@end
