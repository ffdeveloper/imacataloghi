//
//  ICCatalogoCell.m
//  ImaCataloghi
//
//  Created by Francesco Frascà on 12/03/14.
//  Copyright (c) 2014 Francesco Frascà e Alberto Misuraca. All rights reserved.
//

#import "ICCatalogoCell.h"

#import "ICCatalogo.h"

@implementation ICCatalogoCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setStatus:(Status)status
{
    switch (status) {
        case ICStatusToDownload:
            self.labelAttesa.hidden = YES;
            self.labelProgress.hidden = YES;
            self.progressIndicator.hidden = YES;
            self.imageViewStatus.image = [UIImage imageNamed:ICFileNameDownloadImage];
            self.imageViewStatus.hidden = NO;
            break;
        case ICStatusDownloaded:
            if (self.progressIndicator.isAnimating)
                [self.progressIndicator stopAnimating];
            self.labelAttesa.hidden = YES;
            self.labelProgress.hidden = YES;
            self.progressIndicator.hidden = YES;
            self.imageViewStatus.image = [UIImage imageNamed:ICFileNameCheckImage];
            self.imageViewStatus.hidden = NO;
            break;
        case ICStatusInAttesa:
            self.imageViewStatus.hidden = YES;
            self.labelProgress.hidden = YES;
            self.progressIndicator.hidden = YES;
            self.labelAttesa.hidden = NO;
            break;
        case ICStatusDownloading:
            self.imageViewStatus.hidden = YES;
            self.labelAttesa.hidden = YES;
            self.labelProgress.text = @"0.0%";
            self.labelProgress.hidden = NO;
            self.progressIndicator.hidden = NO;
            [self.progressIndicator startAnimating];
            break;
        default:
            break;
    }
    _status = status;
}

- (void)setCatalogo:(ICCatalogo *)catalogo
{
    _catalogo = catalogo;
    self.labelTitolo.text = _catalogo.titolo;
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterNoStyle];
    self.labelData.text = [formatter stringFromDate:_catalogo.data];
    self.status = _catalogo.status;
    [_catalogo addObserver:self forKeyPath:@"currentPercent" options:0 context:nil];
    [_catalogo addObserver:self forKeyPath:@"status" options:0 context:nil];
    [_catalogo addObserver:self forKeyPath:@"alive" options:0 context:nil];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"currentPercent"])
    {
        if (_catalogo.currentPercent == 100) {
            [self setStatus:ICStatusDownloaded];
        } else {
            self.labelProgress.text = [NSString stringWithFormat:@"%.1f%%",_catalogo.currentPercent];
        }
    }
    if ([keyPath isEqualToString:@"status"])
    {
        self.status = _catalogo.status;
    }
    if ([keyPath isEqualToString:@"alive"])
    {
        if (!_catalogo.alive) {
            [_catalogo removeObserver:self forKeyPath:@"currentPercent"];
            [_catalogo removeObserver:self forKeyPath:@"status"];
            [_catalogo removeObserver:self forKeyPath:@"alive"];
        }
    }
}

- (void)dealloc
{
    [_catalogo removeObserver:self forKeyPath:@"currentPercent"];
    [_catalogo removeObserver:self forKeyPath:@"status"];
    [_catalogo removeObserver:self forKeyPath:@"alive"];
}

@end
