//
//  ICCatalogo.h
//  ImaCataloghi
//
//  Created by Francesco Frascà on 12/03/14.
//  Copyright (c) 2014 Francesco Frascà e Alberto Misuraca. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ICConstants.h"

@interface ICCatalogo : NSObject
{
    NSString* _titolo;
    NSDate* _data;
    NSString* _url;
    BOOL _downloaded;
    
    Status _status;
    double _currentPercent;
    
    BOOL _alive;
}

@property (nonatomic, strong) NSString* titolo;
@property (nonatomic, strong) NSString* url;
@property (nonatomic, strong) NSDate* data;
@property (nonatomic, assign) BOOL downloaded;

@property (nonatomic, assign) Status status;
@property (nonatomic, assign) double currentPercent;

@property (nonatomic, assign) BOOL alive;


- (ICCatalogo *)initWithTitolo:(NSString *)titolo url:(NSString *)url data:(NSDate *)data downloaded:(BOOL)downloaded;
- (ICCatalogo *)initWithTitolo:(NSString *)titolo url:(NSString *)url data:(NSDate *)data;
- (ICCatalogo *)initWithDictionary:(NSDictionary *)dictionary;
- (ICCatalogo *)initWithJSONDictionary:(NSDictionary *)dictionary;
- (NSDictionary *)toDictionary;

@end
