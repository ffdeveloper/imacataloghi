//
//  ICCatalogo.m
//  ImaCataloghi
//
//  Created by Francesco Frascà on 12/03/14.
//  Copyright (c) 2014 Francesco Frascà e Alberto Misuraca. All rights reserved.
//

#import "ICCatalogo.h"

@implementation ICCatalogo


- (ICCatalogo *)initWithTitolo:(NSString *)titolo url:(NSString *)url data:(NSDate *)data downloaded:(BOOL)downloaded
{
    if (self = [super init]) {
        _alive = YES;
        _titolo = titolo;
        _data = data;
        _url = url;
        _downloaded = downloaded;
        
        _status = downloaded ? ICStatusDownloaded : ICStatusToDownload;
    }
    return self;
}

- (ICCatalogo *)initWithTitolo:(NSString *)titolo url:(NSString *)url data:(NSDate *)data
{
    return [self initWithTitolo:titolo url:url data:data downloaded:NO];
}

- (ICCatalogo *)initWithDictionary:(NSDictionary *)dictionary
{
    NSString* titolo = dictionary[ICKeyTitolo];
    NSDate* data = dictionary[ICKeyData];
    NSString* url = dictionary[ICKeyURL];
    BOOL downloaded = [dictionary[ICKeyDownloaded] boolValue];
    
    return [self initWithTitolo:titolo url:url data:data downloaded:downloaded];
}

- (ICCatalogo *)initWithJSONDictionary:(NSDictionary *)dictionary
{
    NSString* titolo = dictionary[ICKeyTitolo];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSDate* data = [formatter dateFromString:dictionary[ICKeyData]];
    NSString* url = dictionary[ICKeyURL];
    BOOL downloaded = [dictionary[ICKeyDownloaded] boolValue];
    
    return [self initWithTitolo:titolo url:url data:data downloaded:downloaded];
}

- (NSDictionary *)toDictionary
{
    return  @{ICKeyTitolo:_titolo, ICKeyURL:_url, ICKeyData:_data, ICKeyDownloaded:[NSNumber numberWithBool:_downloaded]};
}

- (void)setStatus:(Status)status
{
    _status = status;
    if (_status == ICStatusDownloaded)
        _downloaded = YES;
}

- (NSString *)description
{
    return [[self toDictionary] description];
}

- (BOOL)isEqual:(id)object
{
    if (!object) return NO;
    if (object == self) return YES;
    if (![object isKindOfClass:[ICCatalogo class]]) return NO;
    ICCatalogo *cat = (ICCatalogo *)object;
    return [cat.titolo isEqualToString:_titolo];
}

@end
