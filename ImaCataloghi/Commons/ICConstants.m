//
//  ICConstants.m
//  ImaCataloghi
//
//  Created by Francesco Frascà on 12/03/14.
//  Copyright (c) 2014 Francesco Frascà e Alberto Misuraca. All rights reserved.
//
#import "ICConstants.h"

@implementation ICConstants

// Dictionary Key
NSString * const ICKeyTitolo = @"titolo";
NSString * const ICKeyData = @"data";
NSString * const ICKeyURL = @"url";
NSString * const ICKeyDownloaded = @"downloaded";

NSString * const ICKeyUserId = @"userID";
NSString * const ICKeyDeviceToken = @"deviceToken";
NSString * const ICKeyRegistered = @"registered";

// File Names
NSString * const ICFileNamePlist = @"Cataloghi";
NSString * const ICFileNamePlistWithExtension = @"Cataloghi.plist";
NSString * const ICFileNameDownloadImage = @"download.png";
NSString * const ICFileNameCheckImage = @"check.png";

// URL
NSString * const ICURLApi = @"api/api.php";


@end