//
//  ICAppDelegate.m
//  ImaCataloghi
//
//  Created by Francesco Frascà on 12/03/14.
//  Copyright (c) 2014 Francesco Frascà e Alberto Misuraca. All rights reserved.
//

#import "ICAppDelegate.h"

//Push
#import "ICConstants.h"
#import <ASIHTTPRequest/ASIFormDataRequest.h>

@implementation ICAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        UISplitViewController *splitViewController = (UISplitViewController *)self.window.rootViewController;
        UINavigationController *navigationController = [splitViewController.viewControllers lastObject];
        splitViewController.delegate = (id)navigationController.topViewController;
    }
    
    // PUSH
    // Let the device know we want to receive push notifications
	[[UIApplication sharedApplication] registerForRemoteNotificationTypes:
     (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    
    if (launchOptions)
	{
		NSDictionary *dictionary = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
		if (dictionary)
		{
			NSLog(@"Launched from push notification: %@", dictionary);
		}
	}
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 1];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark -
#pragma mark Notifiche Push

// PUSH

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    if (![self registered]) {
        [self postJoinRequest];
    }
    
	NSString *oldToken = [self deviceToken];
    
	NSString *newToken = [deviceToken description];
	newToken = [newToken stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
	newToken = [newToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    
	NSLog(@"My token is: %@", newToken);
    
	[self setDeviceToken:newToken];
    
	if ([self registered] && ![newToken isEqualToString:oldToken])
	{
		[self postUpdateRequest];
	}

}

- (void)postJoinRequest {
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[MAIN_URL stringByAppendingString:ICURLApi]]];
    [request setPostValue:@"join" forKey:@"cmd"];
    [request setPostValue:[self deviceToken] forKey:@"token"];
    [request setPostValue:[self userId] forKey:@"user_id"];
    
    request.delegate = self;
    [request startAsynchronous];
    
}

- (void)postUpdateRequest
{
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[MAIN_URL stringByAppendingString:ICURLApi]]];
    [request setPostValue:@"update" forKey:@"cmd"];
    [request setPostValue:[self userId] forKey:@"user_id"];
    [request setPostValue:[self deviceToken] forKey:@"token"];
    [request startAsynchronous];
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
	NSLog(@"Failed to get token, error: %@", error);
}

- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary*)userInfo
{
	NSLog(@"Received notification: %@", userInfo);
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 1];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
}

#pragma mark -
#pragma mark ASIRequest Delegate
- (void)requestFinished:(ASIHTTPRequest *)request
{
    if(request.responseStatusCode != 200) {
        NSLog(@"There was an error communicating with the server: %i", request.responseStatusCode);
        NSLog(@"%@", request.responseHeaders);
        NSLog(@"%@", request.responseString);
    } else {
        [self setRegistered:YES];
        NSLog(@"Registrato correttamente");
        NSLog(@"%@", request.responseHeaders);
        NSLog(@"%@", request.responseString);
    }
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    NSLog(@"Errore nella richiesta di registrazione");
}

#pragma mark -
#pragma mark Utility Method

- (BOOL)registered
{
	return [[NSUserDefaults standardUserDefaults] boolForKey:ICKeyRegistered];
}

- (void)setRegistered:(BOOL)value
{
	[[NSUserDefaults standardUserDefaults] setBool:value forKey:ICKeyRegistered];
    
}

- (NSString*)userId
{
    NSString *userId = [[NSUserDefaults standardUserDefaults] stringForKey:ICKeyUserId];
    if (userId == nil || userId.length == 0) {
        userId = [[[NSUUID UUID] UUIDString] stringByReplacingOccurrencesOfString:@"-" withString:@""];
        [[NSUserDefaults standardUserDefaults] setObject:userId forKey:ICKeyUserId];
    }
    return userId;
}

- (NSString*)deviceToken
{
	return [[NSUserDefaults standardUserDefaults] stringForKey:ICKeyDeviceToken];
}

- (void)setDeviceToken:(NSString*)token
{
	[[NSUserDefaults standardUserDefaults] setObject:token forKey:ICKeyDeviceToken];
}

@end
