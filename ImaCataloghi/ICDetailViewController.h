//
//  ICDetailViewController.h
//  ImaCataloghi
//
//  Created by Francesco Frascà on 11/03/14.
//  Copyright (c) 2014 Francesco Frascà e Alberto Misuraca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReaderViewController.h"
@class ICCatalogo;

@interface ICDetailViewController : UIViewController <UISplitViewControllerDelegate, ReaderViewControllerDelegate>

@property (strong, nonatomic) ICCatalogo *catalogo;
@property (nonatomic) BOOL  hide;

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
//@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
@end
