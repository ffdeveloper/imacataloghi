//
//  ICMasterViewController.m
//  ImaCataloghi
//
//  Created by Francesco Frascà on 11/03/14.
//  Copyright (c) 2014 Francesco Frascà e Alberto Misuraca. All rights reserved.
//

#import "ICMasterViewController.h"

#import "ICDetailViewController.h"

#import "ICCatalogo.h"
#import "ICCatalogoCell.h"

#import "ICConstants.h"

#import <ASIHTTPRequest/ASIFormDataRequest.h>

#define STOP_DOWNLOADING_TAG 100
#define REMOVE_WAITING_TAG 200

@interface ICMasterViewController () {
    NSMutableArray *_cataloghi;
    BOOL _started;
    
    // Download
    NSURLSessionDownloadTask *_downloadTask;
    ICCatalogo *_currentDownloading;
    ICCatalogo *_removeFromWaiting;
    BOOL _annulla;
    BOOL _aggiornato;
    UIAlertView *_alertView;
}
@end

@implementation ICMasterViewController

- (void)awakeFromNib
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        self.clearsSelectionOnViewWillAppear = NO;
        self.preferredContentSize = CGSizeMake(320.0, 600.0);
    }
    [super awakeFromNib];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.detailViewController = (ICDetailViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];
    
    _cataloghi = [NSMutableArray array];
    NSString *errorDesc = nil;
    NSPropertyListFormat format;
    NSString *plistPath;
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                              NSUserDomainMask, YES) objectAtIndex:0];
    plistPath = [rootPath stringByAppendingPathComponent:ICFileNamePlistWithExtension];
    if (![[NSFileManager defaultManager] fileExistsAtPath:plistPath]) {
        NSString *localPlistPath = [[NSBundle mainBundle] pathForResource:ICFileNamePlist ofType:@"plist"];
        [[NSFileManager defaultManager] copyItemAtPath:localPlistPath toPath:plistPath error:nil];
    }
    NSData *plistXML = [[NSFileManager defaultManager] contentsAtPath:plistPath];
    NSArray *temp = (NSArray *)[NSPropertyListSerialization
                                          propertyListFromData:plistXML
                                          mutabilityOption:NSPropertyListMutableContainersAndLeaves
                                          format:&format
                                          errorDescription:&errorDesc];
    if (!temp) {
        NSLog(@"Error reading plist: %@, format: %u", errorDesc, format);
    } else {
        for (NSDictionary *dictionary in temp) {
            [_cataloghi addObject:[[ICCatalogo alloc] initWithDictionary:dictionary]];
        }
        [self sort];
    }
    if (!_started && [_cataloghi count] > 0) {
        for (int i = 0; i < [_cataloghi count]; i++) {
            ICCatalogo *cat = _cataloghi[i];
            if (cat.downloaded) {
                [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
                
                if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
                    [self performSegueWithIdentifier:@"showDetail" sender:nil];
                else
                    self.detailViewController.catalogo = _cataloghi[i];
                break;
            }
        }
    }
    _started = YES;
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[MAIN_URL stringByAppendingString:ICURLApi]]];
    [request setPostValue:@"json" forKey:@"cmd"];
    [request setPostValue:@"ima_cataloghi" forKey:@"user_name"];
    [request setPostValue:@"password_cataloghi_ima" forKey:@"password"];
    request.delegate = self;
    [request startAsynchronous];
//    self.navigationItem.leftBarButtonItem.enabled = NO;

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _cataloghi.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ICCatalogoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];

    ICCatalogo *catalogo = _cataloghi[indexPath.row];
    cell.catalogo = catalogo;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ICCatalogo *catalogo = _cataloghi[indexPath.row];
    if (catalogo.downloaded) {
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
            self.detailViewController.catalogo = catalogo;
    } else {
        if (catalogo.status == ICStatusToDownload) {
            if (!_currentDownloading) {
                _currentDownloading = catalogo;
                catalogo.status = ICStatusDownloading;
                NSURL *URL = [NSURL URLWithString:catalogo.url];
                NSURLRequest *request = [NSURLRequest requestWithURL:URL];
                NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
                NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:nil];
                _downloadTask =[session downloadTaskWithRequest:request];
                _annulla = NO;
                [_downloadTask resume];
            } else {
                catalogo.status = ICStatusInAttesa;
            }
        } else if (catalogo.status == ICStatusDownloading) {
            _alertView = [[UIAlertView alloc] initWithTitle:@"Attenzione" message:@"Vuoi interrompere il download del catalogo?" delegate:self cancelButtonTitle:@"No, continua" otherButtonTitles:@"Si", nil];
            _alertView.tag = STOP_DOWNLOADING_TAG;
            [_alertView show];
        } else if (catalogo.status == ICStatusInAttesa) {
            _removeFromWaiting = catalogo;
            _alertView = [[UIAlertView alloc] initWithTitle:@"Attenzione" message:@"Vuoi rimuovere questo catalogo dai download in attesa?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Si", nil];
            _alertView.tag = REMOVE_WAITING_TAG;
            [_alertView show];
        }
    }
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if ([identifier isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        ICCatalogo *catalogo = _cataloghi[indexPath.row];
        return catalogo.downloaded;
    }
    return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        ICCatalogo *catalogo = _cataloghi[indexPath.row];
        [[segue destinationViewController] setCatalogo:catalogo];
    }
}


#pragma NSURLSessionDownload Delegate
- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite
{
    double percentuale = (double)totalBytesWritten * 100;
    percentuale /= totalBytesExpectedToWrite;
    [[NSOperationQueue mainQueue] addOperationWithBlock:^
     {
         _currentDownloading.currentPercent = percentuale;
     }];
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location
{
    [_alertView dismissWithClickedButtonIndex:0 animated:YES];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    NSURL *pdfUrl = [NSURL fileURLWithPath:[[documentsPath stringByAppendingPathComponent:_currentDownloading.titolo] stringByAppendingPathExtension:@"pdf"]];
    NSError *error = nil;
    NSFileManager *fm = [NSFileManager defaultManager];
    if ([fm fileExistsAtPath:[pdfUrl path]])
    {
        [fm removeItemAtPath:[pdfUrl path] error:&error];
    }
    if (error)
    {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^
         {
             [self reset];
             _currentDownloading = nil;
             UIAlertView* msgbox = [[UIAlertView alloc] initWithTitle:@"ERRORE" message:@"Errore nell'eliminazione del file pdf precedentemente scaricato" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
             [msgbox show];
         }];
        return;
    }
    [fm copyItemAtURL:location toURL:pdfUrl error:&error];
    if (error)
    {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^
         {
             [self reset];
             _currentDownloading = nil;
             UIAlertView* msgbox = [[UIAlertView alloc] initWithTitle:@"ERRORE" message:@"Errore nella copia del file in locale" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
             [msgbox show];
         }];
        return;
    }
    [[NSOperationQueue mainQueue] addOperationWithBlock:^
     {
         _currentDownloading.currentPercent = 100;
         _currentDownloading.status = ICStatusDownloaded;
         [self scrivi];
         _currentDownloading = nil;
         if ([self cataloghiInAttesa]) {
             for (ICCatalogo *cat in _cataloghi) {
                 if (cat.status == ICStatusInAttesa) {
                     _currentDownloading = cat;
                     cat.status = ICStatusDownloading;
                     NSURL *URL = [NSURL URLWithString:cat.url];
                     NSURLRequest *request = [NSURLRequest requestWithURL:URL];
                     NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
                     NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:nil];
                     _downloadTask =[session downloadTaskWithRequest:request];
                     _annulla = NO;
                     [_downloadTask resume];
                     return;
                 }
             }
         }
     }];
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didResumeAtOffset:(int64_t)fileOffset expectedTotalBytes:(int64_t)expectedTotalBytes {
    
}

- (void)URLSession:(NSURLSession *)session didBecomeInvalidWithError:(NSError *)error
{
    [_alertView dismissWithClickedButtonIndex:0 animated:YES];
    if (!_annulla) {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^
         {
             [self reset];
             _currentDownloading = nil;
             UIAlertView* msgbox = [[UIAlertView alloc] initWithTitle:@"ERRORE" message:@"Errore nel download del file, riprovare" delegate:self cancelButtonTitle:@"Chiudi" otherButtonTitles: nil];
             [msgbox show];
         }];
    } else if (_annulla) {
        if ([self cataloghiInAttesa]) {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^
             {
                 for (ICCatalogo *cat in _cataloghi) {
                     if (cat.status == ICStatusInAttesa) {
                         _currentDownloading = cat;
                         cat.status = ICStatusDownloading;
                         NSURL *URL = [NSURL URLWithString:cat.url];
                         NSURLRequest *request = [NSURLRequest requestWithURL:URL];
                         NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
                         NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:nil];
                         _downloadTask =[session downloadTaskWithRequest:request];
                         _annulla = NO;
                         [_downloadTask resume];
                         return;
                     }
                 }
             }];
        }
    }
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error
{
    [_alertView dismissWithClickedButtonIndex:0 animated:YES];
    if (error && !_annulla) {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^
         {
             NSLog(@"%@", [error localizedDescription]);
             [self reset];
             _currentDownloading = nil;
             UIAlertView* msgbox = [[UIAlertView alloc] initWithTitle:@"ERRORE" message:@"Errore nel download del file, riprovare" delegate:self cancelButtonTitle:@"Chiudi" otherButtonTitles: nil];
             [msgbox show];
         }];
    }
    if (_annulla) {
        if ([self cataloghiInAttesa]) {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^
             {
                 for (ICCatalogo *cat in _cataloghi) {
                     if (cat.status == ICStatusInAttesa) {
                         _currentDownloading = cat;
                         cat.status = ICStatusDownloading;
                         NSURL *URL = [NSURL URLWithString:cat.url];
                         NSURLRequest *request = [NSURLRequest requestWithURL:URL];
                         NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
                         NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:nil];
                         _downloadTask =[session downloadTaskWithRequest:request];
                         _annulla = NO;
                         [_downloadTask resume];
                         return;
                     }
                 }
             }];
        }
    }
}

#pragma mark -
#pragma mark AlertView Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        if (alertView.tag == STOP_DOWNLOADING_TAG) {
            _annulla = YES;
            if (_downloadTask) {
                [_downloadTask cancel];
                _currentDownloading.currentPercent = 0;
                _currentDownloading.status = ICStatusToDownload;
                _currentDownloading = nil;
                _downloadTask = nil;
            }
        } else if (alertView.tag == REMOVE_WAITING_TAG) {
            _removeFromWaiting.status = ICStatusToDownload;
            _removeFromWaiting = nil;
        }
    }
}


#pragma mark -
#pragma mark ASIRequest Delegate
- (void)requestFinished:(ASIHTTPRequest *)request
{
    NSError *error;
    NSArray *temp = [NSJSONSerialization JSONObjectWithData:[request responseData] options:0 error:&error];
    if (error) {
        NSLog(@"%@", request.responseString);
        [[[UIAlertView alloc] initWithTitle:@"Errore" message:@"L'applicazione non è riuscita a ottenere informazioni sui cataloghi dal server" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
        NSLog(@"%@", error);
        return;
    }
    NSMutableArray *temp2 = [NSMutableArray arrayWithCapacity:[temp count]];
    for (NSDictionary *dict in temp) {
        [temp2 addObject:[[ICCatalogo alloc] initWithJSONDictionary:dict]];
    }
    for (ICCatalogo *cat in _cataloghi) {
        cat.alive = NO;
    }
    
    for (ICCatalogo *cat in temp2) {
        ICCatalogo *old = [self catalogoWithTitolo:cat.titolo];
        if (old) {
            cat.status = old.status;
        }
    }
    
    _cataloghi = temp2;
    [self sort];
    [self scrivi];
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^
     {
         [self.tableView reloadData];
     }];
}

- (void)requestFailed:(ASIHTTPRequest *)request
{    NSError *error = [request error];
    NSLog(@"%@", error);
    if (_aggiornato) {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^
         {
             [[[UIAlertView alloc] initWithTitle:@"Errore" message:@"Il server non è al momento disponibile, riprova più tardi" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
         }];
    }
}


#pragma mark -
#pragma mark Utility methods
- (BOOL)cataloghiInAttesa
{
    for (ICCatalogo *cat in _cataloghi) {
        if (cat.status == ICStatusInAttesa) {
            return YES;
        }
    }
    return NO;
}

- (void)reset
{
    for (ICCatalogo *cat in _cataloghi) {
        cat.status = cat.downloaded ? ICStatusDownloaded : ICStatusToDownload;
    }
}

- (void)sort
{
    [_cataloghi sortUsingComparator:^(ICCatalogo *a, ICCatalogo *b) {
        return [b.data compare:a.data];
    }];
}

- (void)scrivi
{
    NSMutableArray *temp = [NSMutableArray arrayWithCapacity:[_cataloghi count]];
    for (ICCatalogo *cat in _cataloghi) {
        [temp addObject:[cat toDictionary]];
    }
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                              NSUserDomainMask, YES) objectAtIndex:0];
    NSString *plistPath = [rootPath stringByAppendingPathComponent:ICFileNamePlistWithExtension];
    [temp writeToFile:plistPath atomically:NO];
}

- (ICCatalogo *)catalogoWithTitolo:(NSString *)titolo
{
    for (ICCatalogo *cat in _cataloghi) {
        if ([cat.titolo isEqualToString:titolo]) {
            return cat;
        }
    }
        
    return nil;
}

#pragma mark -
#pragma mark IBAction

- (void)aggiorna:(id)sender
{
    _aggiornato = YES;
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[MAIN_URL stringByAppendingString:ICURLApi]]];
    [request setPostValue:@"json" forKey:@"cmd"];
    [request setPostValue:@"ima_cataloghi" forKey:@"user_name"];
    [request setPostValue:@"password_cataloghi_ima" forKey:@"password"];
    request.delegate = self;
    [request startAsynchronous];
}




@end
