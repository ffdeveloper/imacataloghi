//
//  ICMasterViewController.h
//  ImaCataloghi
//
//  Created by Francesco Frascà on 11/03/14.
//  Copyright (c) 2014 Francesco Frascà e Alberto Misuraca. All rights reserved.
//

#import <UIKit/UIKit.h>

// JSON
#import <ASIHTTPRequest/ASIHTTPRequest.h>

@class ICDetailViewController;

@interface ICMasterViewController : UITableViewController <NSURLSessionDownloadDelegate, UIAlertViewDelegate, ASIHTTPRequestDelegate>

@property (strong, nonatomic) ICDetailViewController *detailViewController;


- (IBAction)aggiorna:(id)sender;

@end
